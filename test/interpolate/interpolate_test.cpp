#include "interpolate_test.h"

#include <QString>
#include <QtTest>

#include <interpolate/newton.h>
#include <interpolate/lagrange.h>

InterpolateTest::InterpolateTest()
{
    Model::Parseval::SymbolTable* table = Model::Parseval::SymbolTable::getInstance();
    table->insertSymbol("var", QRegExp("^(x|y)"), Model::Parseval::ARITY_CONSTANT, Model::Parseval::ASSOCIATIVITY_NONE, 0, 0);
    table->insertSymbol("num", QRegExp("^[0-9]+(\\.[0-9]+)?"), Model::Parseval::ARITY_CONSTANT, Model::Parseval::ASSOCIATIVITY_NONE, 0, 0);
    table->insertSymbol("open", QRegExp("^\\("), Model::Parseval::ARITY_CONSTANT, Model::Parseval::ASSOCIATIVITY_NONE, 0, 0);
    table->insertSymbol("close", QRegExp("^\\)"), Model::Parseval::ARITY_CONSTANT, Model::Parseval::ASSOCIATIVITY_NONE, 0, 0);

    table->insertSymbol("add", QRegExp("^\\+"), Model::Parseval::ARITY_BINARY, Model::Parseval::ASSOCIATIVITY_LEFT, 1, [](double a, double b){ return a + b; });
    table->insertSymbol("min", QRegExp("^-"), Model::Parseval::ARITY_BINARY, Model::Parseval::ASSOCIATIVITY_LEFT, 1, [](double a, double b){ return a - b; });
    table->insertSymbol("mul", QRegExp("^\\*"), Model::Parseval::ARITY_BINARY, Model::Parseval::ASSOCIATIVITY_LEFT, 2, [](double a, double b){ return a * b; });
    table->insertSymbol("div", QRegExp("^/"), Model::Parseval::ARITY_BINARY, Model::Parseval::ASSOCIATIVITY_LEFT, 2, [](double a, double b){ return a / b; });
    table->insertSymbol("pow", QRegExp("^\\^"), Model::Parseval::ARITY_BINARY, Model::Parseval::ASSOCIATIVITY_RIGHT, 3, [](double a, double b){ return pow(a, b); });

    table->insertSymbol("abs", QRegExp("^abs"), Model::Parseval::ARITY_UNARY, Model::Parseval::ASSOCIATIVITY_NONE, 0, [](double a, double){ return a<0?-a:a; });

    table->insertSymbol("sin", QRegExp("^sin"), Model::Parseval::ARITY_UNARY, Model::Parseval::ASSOCIATIVITY_NONE, 0, [](double a, double){ return sin(a); });
    table->insertSymbol("cos", QRegExp("^cos"), Model::Parseval::ARITY_UNARY, Model::Parseval::ASSOCIATIVITY_NONE, 0, [](double a, double){ return cos(a); });
    table->insertSymbol("tg", QRegExp("^tg"), Model::Parseval::ARITY_UNARY, Model::Parseval::ASSOCIATIVITY_NONE, 0, [](double a, double){ return tan(a); });
    table->insertSymbol("ctg", QRegExp("^ctg"), Model::Parseval::ARITY_UNARY, Model::Parseval::ASSOCIATIVITY_NONE, 0, [](double a, double){ return 1 / tan(a); });
}

void InterpolateTest::LagrangeTest()
{
    Model::Interpolate::Lagrange lagrange;

    lagrange.addData(3, 9, 6);
    lagrange.addData(3, 5, -4);
    lagrange.addData(6, 5, 0);
    lagrange.addData(6, 9, 7);

    lagrange.initialize();
    QCOMPARE(lagrange.calculate(3, 9), 6.0);
    QCOMPARE(lagrange.calculate(3, 5), -4.0);
    QCOMPARE(lagrange.calculate(6, 5), 0.0);
    QCOMPARE(lagrange.calculate(6, 9), 7.0);
}

void InterpolateTest::NewtonTest()
{
    Model::Interpolate::Newton newton;

    newton.addData(-10, 0, 6);
    newton.addData(-3, 0, -4);
    newton.addData(8, 0, 1);

    newton.initialize();
    QCOMPARE(newton.calculate(-10, 0), 6.0);
    QCOMPARE(newton.calculate(-3, 0), -4.0);
    QCOMPARE(newton.calculate(8, 0), 1.0);
}

QTEST_APPLESS_MAIN(InterpolateTest)
