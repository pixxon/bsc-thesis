#include <QObject>

class InterpolateTest : public QObject
{
    Q_OBJECT

public:
    InterpolateTest();

private Q_SLOTS:
    void LagrangeTest();
    void NewtonTest();
};
