project(parseval_test)

find_package(Qt5 COMPONENTS Test REQUIRED)

qt5_wrap_cpp(MOC_SRCS
	parseval_test.h
)

set(HEADERS
	parseval_test.h
)

set(SOURCES
	parseval_test.cpp
)

add_executable(${PROJECT_NAME} ${HEADERS} ${SOURCES} ${MOC_SRCS})
add_test(${PROJECT_NAME} ${PROJECT_NAME})

target_link_libraries(${PROJECT_NAME} Qt5::Test parseval)
