#pragma once

#include <QObject>

class ParsevalTest : public QObject
{
    Q_OBJECT

public:
    ParsevalTest();

private Q_SLOTS:
    void SymbolTest();
    void TokenizerTest();
    void ParserTest();
    void EvaluatorTest();
};
